CKEDITOR.editorConfig = function( config ) {

    // Ref: http://ckeditor.com/latest/samples/plugins/toolbar/toolbar.html

    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.plugins = 'dialogui,dialog,about,a11yhelp,basicstyles,blockquote,button,toolbar,clipboard,panel,floatpanel,menu,contextmenu,resize,elementspath,enterkey,entities,popup,filebrowser,floatingspace,listblock,richcombo,format,horizontalrule,htmlwriter,wysiwygarea,image,indent,indentlist,fakeobjects,link,list,magicline,maximize,pastetext,removeformat,sourcearea,specialchar,menubutton,scayt,stylescombo,tab,table,undo,justify';
    config.toolbarGroups = [
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
        { name: 'links' },
        { name: 'insert' },
        { name: 'forms' },
        { name: 'tools' },
        { name: 'document',    groups: [ 'document', 'doctools' ] },
        { name: 'others' },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        { name: 'styles' },
        { name: 'colors' },
        { name: 'more', groups: [ 'mode' ] }
    ];

    config.language = editorLocale;

    config.uiColor = '#e6e6e6';

    config.extraPlugins = 'flags,templates,images';
};
