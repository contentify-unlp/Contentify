<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Module Language Lines
    |--------------------------------------------------------------------------
    |
    | This file keeps the language lines of the related module.
    |
    */

    // Cup - torneo
    'join_at'            => 'Registro',
    'check_in_at'        => 'Check In',
    'start_at'           => 'Comienzo',
    'checked_in'         => 'Check in realizado',
    'seeding'            => 'Generar brackets',
    'players_per_team'   => 'Jugadores por equipo',
    'bracket'            => 'Bracket',
    'cup_full'           => 'Número máximo de participantes alcanzado.',
    'cannot_join'        => 'El registro al torneo todavía no se ha abierto - por favor, espere a que la fase de registro se abra.',
    'joined'             => 'Ya estás participando. Por favor, espera a que la fase de check in se abra.',
    'join_hint'          => 'Haz clic aquí para unirte al torneo:',
    'join'               => 'Unirse ahora',
    'no_team'            => 'No tienes aquipo para unirte al torneo!',
    'create_team'        => 'Crear un equipo',
    'check_out'          => 'LISTO PARA JUGAR. Clic aquí para salir',
    'check_in'           => 'Clic aquí para realizar el check in:',
    'check_out_not_org'  => 'Solo un organizador puede realizar el check out',
    'check_in_not_org'   => 'Solo un organizador puede realizar el check in',
    'not_participating'  => 'No estás participando en este torneo.',
    'cup_started'        => 'El torneo ha comenzado. No puede realizar el check in.',
    'cup_running'        => 'El torneo ya ha empezado. Por favor, mira los brackets para ver la clasificación.',
    'cup_closed'         => 'El torneo ya se ha cerrado.',
    'login_hint'         => '¡Identifícate para tener acceso a este torneo!',
    'referees'           => 'Árbitros',
    'swap'               => 'Cambiar',
    'in_no_cups'         => 'No estás registrado en ningún torneo.',
    'join_a_cup'         => '¡Únete al torneo ahora!',
    'check_in_succesfull' => '¡El check in se realizó de forma exitosa!',
    'check_out_succesfull'=> 'Se ha realizado el check out al torneo',

    // Team
    'not_organizer'      => 'Sólo un miembro de tu equipo, con permisos de Organizador, está capacitado para hacer esto.',
    'organizer'          => 'Organizador',
    'user_conflict'      => 'Un usuario no puede estar en más de un equipo por torneo.',
    'team_locked'        => 'El equipo se bloquea en el transcurso de un torneo. No esttá permitido hacer cambios.',
    'edit_team'          => 'Editar el equipo',
    'team_deleted'       => 'Este equipo ha sido borrado.',
    'my_teams'           => 'Mis equipos',
    'wrong_password'     => '¡contraseña incorrecta! Por favor, vuelvelo a intentar.',
    'min_organizers'     => 'Esto no es posible. El equipo debe tener al menos un organizador.',
    'not_enough_players' => 'Muy pocos jugadores',
    'joinTeam'           => 'Unirse',
    'searchTeam'         => 'Buscar equipo',
    'teamMember'         => 'Miembro del equipo',
    'check_out_not_organizer' => 'LISTOS PARA JUGAR. Solo un jugador con permisos de organizador está capacitado para realizar el check out ',
    'check_in_not_organizer' => 'Sólo un miembro de tu equipo, con permisos de Organizador, está capacitado para hacer realizar el check in.',

    // Match
    'confirm_score'      => 'Confirmar resultados',
    'new_match'          => 'Se ha generado un nuevo encuentro!',
    'change_winner'      => 'Cambiar el ganador',
    'next_match'         => 'Siguiente partido',
    'tie_match'          => 'No se pueden generar empates en la copa',
    'letter_confirm'     => 'Sólo se pueden ingresar números como resultados',

];
