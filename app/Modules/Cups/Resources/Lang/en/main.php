<?php 

return [

    /*
    |--------------------------------------------------------------------------
    | Module Language Lines
    |--------------------------------------------------------------------------
    |
    | This file keeps the language lines of the related module.
    |
    */
    
    // Cup
    'join_at'            => 'Join At',
    'check_in_at'        => 'Check In At',
    'start_at'           => 'Start At',
    'checked_in'         => 'Checked In',
    'seeding'            => 'Seeding',
    'players_per_team'   => 'Players per Team',
    'bracket'            => 'Bracket',
    'cup_full'           => 'Maximal number of participants reached.',
    'cannot_join'        => 'The cup registration is not open yet - please wait until the join phase starts.',
    'joined'             => 'You are participating. Please wait for the check-in phase.',
    'join_hint'          => 'Click here to join the cup:',
    'join'               => 'Join now',
    'no_team'            => 'You have no team to join the cup!',
    'create_team'        => 'Create team',
    'check_out'          => 'Click here to check-out:',
    'check_in'           => 'Click here to check-in:',
    'check_out_not_org'  => 'Only an organizer can check-out',
    'check_in_not_org'   => 'Only an organizer can check-in',
    'not_participating'  => 'You are not participating in this cup.',
    'cup_started'        => 'The cup has started. You can\'t check-in.',
    'cup_running'        => 'The cup is running. Please check the brackets to see the matches.',
    'cup_closed'         => 'The cup has been closed.',
    'login_hint'         => 'Login to get access to this cup!',
    'referees'           => 'Referees',
    'swap'               => 'Swap',
    'in_no_cups'         => 'You are not registered for any cups.',
    'join_a_cup'         => 'Join a cup now!',

    // Team
    'not_organizer'      => 'Only a team member with organizer permissions is allowed to do that.',
    'organizer'          => 'Organizer',
    'user_conflict'      => 'A user cannot be in more than one team per cup.',
    'team_locked'        => 'The team is locked while it participates in a cup. No changes can be made.',
    'edit_team'          => 'Edit team',
    'team_deleted'       => 'This team has been deleted.',
    'my_teams'           => 'My teams',
    'wrong_password'     => 'Wrong password! Please try again.',
    'min_organizers'     => 'This is not possible. The team must have at least one organizer.',
    'not_enough_players' => 'Not enough players',
    'joinTeam'           => 'Join',
    'searchTeam'         => 'Search team',
    'teamMember'         => 'Member of team',

    // Match
    'confirm_score'      => 'Confirm Score',
    'new_match'          => 'A new match has been generated!',
    'change_winner'      => 'Change winner',
    'next_match'         => 'Next Match',
    'tie_match'          => 'A match can\'t be tied in the cup',
    'letter_confirm'     => 'Only numbers can be input for match result',

];
