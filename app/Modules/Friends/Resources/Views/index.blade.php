<h1 class="page-title">{{ trans_object('friends') }} {{ trans('app.of') }} {{ $user->username }}</h1>

<a class="btn btn-default searchUser" href="{{ url('friends') }}">{!! HTML::fontIcon('search') !!} {{ trans('friends::searchUser') }}</a>

@foreach ($user->friends() as $friend)
    @include('friends::friend', compact('friend'))
@endforeach
