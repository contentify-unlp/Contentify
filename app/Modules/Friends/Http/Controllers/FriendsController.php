<?php

namespace App\Modules\Friends\Http\Controllers;

use App\Modules\Friends\Friendship;
use DB;
use FrontController;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Raw;
use Redirect;
use User;
use HTML;
use App\Modules\Users\User as UserDB;

class FriendsController extends FrontController
{

    public function index()
    {
        $this->modelClass = UserDB::class;

        $this->pageView('friends::filter_friend');

        $this->indexPage([
            'buttons'       => null,
            'brightenFirst' => false,
            'filter'        => true,
            'searchFor' => 'username',
            'tableHead'     => [
                trans('app.username')              => 'username',
                trans('app.name')                  => 'realName',
                trans('users::add_friend')         => 'addFriend',
                trans('friends::reject')           => 'reject',
            ],
            'tableRow'      => function(UserDB $user)
            {
                if($user->banned){
                    return [];
                }

                $addFriend = null;
                $friendship = null;
                if($user->id != user()->id) {
                    $friendship = Friendship::areFriends(user()->id, $user->id, false)->first();
                    $addFriend = $this->checkFriendshipState($friendship, $user->id);
                }
                $realName = (is_null($user->first_name) && is_null($user->first_name)) ? " - ": $user->getRealName();

                $canDelete = ($this->canDelete($friendship, user()->id)) ? raw(HTML::link(url('friends/'.$friendship->sender->id.'?method=DELETE&_token='.csrf_token().'&redirectTo=friends'), trans('app.delete'))):null;

                return [
                    raw(HTML::link(url("users/{$user->id}/{$user->slug}"), $user->username)),
                    $realName,
                    $addFriend,
                    $canDelete,
                ];
            },
            'actions'       => null,
            'pageTitle'     => false,
        ], 'front');
    }

    /**
     * Shows the friends of a user
     *
     * @param  int $id The ID of the user
     * @return mixed
     * @throws \Exception
     */
    public function show(int $id)
    {
        $user = User::findOrFail($id);

        $this->pageView('friends::index', compact('user'));
    }

    /**
     * Tries to send a friendship request to a user
     * 
     * @param int $id The ID if the user
     * @param  Request $request
     * @return RedirectResponse
     */
    public function add(Request $request, int $id)
    {
        /** @var User $friend */
        $friend = User::findOrFail($id);

        /** @var Friendship $friendship */
        $friendship = Friendship::areFriends(user()->id, $id, false)->first();

        $friendshipImpossible = ($friendship and 
            ($friendship->confirmed or
             $friendship->messaged_at->timestamp > time() - Friendship::FRIENDSHIP_REQUEST_LIFESPAN));

        if ($friendshipImpossible or user()->id == $friend->id) {
            $this->alertFlash(trans('friends::request_error'));
            return Redirect::to('users/'.$friend->id);
        }

        if ($friendship) {
            DB::table('friends')->whereSenderId($friendship->sender_id)->whereReceiverId($friendship->receiver_id)
                ->update(['messaged_at' => DB::raw('NOW()')]);
        } else {
            DB::table('friends')->insert([
                'sender_id'     => user()->id, 
                'receiver_id'   => $friend->id,
                'messaged_at'   => DB::raw('NOW()'),
            ]);
        }

        $friend->sendSystemMessage(
            trans('friends::request_title'),
            trans('friends::request_text', [user()->username]).link_to('friends/confirm/'.user()->id, 'Accept')
        );

        $this->alertFlash(trans('friends::request_sent', [$friend->username]));

        $redirectTo = $request->input("redirectTo");
        if(! is_null($redirectTo)){
            return Redirect::to($redirectTo);
        }
        return Redirect::to('users/'.$id);
    }

    /**
     * Confirms a friendship
     * 
     * @param  int $id The ID of the user
     * @return RedirectResponse
     */
    public function confirm(Request $request, int $id) : RedirectResponse
    {
        /** @var User $friend */
        $friend = User::findOrFail($id);

        /** @var Friendship $friendship */
        $friendship = Friendship::areFriends(user()->id, $id, false)->first();

        if (! $friendship or $friendship->confirmed) {
            $this->alertFlash(trans('friends::request_error'));
            return Redirect::to('users/'.$friend->id);
        }

        DB::table('friends')->whereSenderId($friendship->sender_id)->whereReceiverId($friendship->receiver_id)
                ->update(['confirmed' => 1]);

        $this->alertFlash(trans('friends::request_accepted'));

        $redirectTo = $request->input("redirectTo");
        if(! is_null($redirectTo)){
            return Redirect::to($redirectTo);
        }

        return Redirect::to('users/'.$id);
    }

    /**
     * Destroys a friendship (= destroys the user-to-user-relationship)
     * 
     * @param  int $id The ID if the user (friend)
     * @return RedirectResponse
     */
    public function destroy(Request $request, int $id) : RedirectResponse
    {
        /** @var User $friend */
        $friend = User::findOrFail($id);
        
        // NOTE: Only receive confirmed friendships.
        // It's important that users can only delete confirmed friendship to avoid abuse for friendship request spamming
        /** @var Friendship $friendship */
        $friendship = Friendship::areFriends(user()->id, $id)->firstOrFail();

        if ($friendship) {
            DB::table('friends')->whereSenderId($friendship->sender_id)->whereReceiverId($friendship->receiver_id)
                ->delete();

            $friend->sendSystemMessage(
                trans('friends::deletion_title'),
                trans('friends::deletion_text', [user()->username])
            );
        }

        $this->alertFlash(trans('app.deleted', ['Friendship'])); // Friendship terminated. Take this, diction!

        $redirectTo = $request->input("redirectTo");
        if(! is_null($redirectTo)){
            return Redirect::to($redirectTo);
        }

        return Redirect::to('friends/'.user()->id);
    }

    /**
     * Check if friendship exists and return the state.
     *
     * @param  Friendship $friendship The friendshio to check.
     * @param  string $userId   The id of the user for the URLs.
     * @return string|Raw
     */
    public function checkFriendshipState($friendship, string $userId)
    {

        if(is_null($friendship)){
            return raw(HTML::link(url('friends/add/'.$userId.'?redirectTo=friends'), trans('app.add')));
        }

        if(!$friendship->confirmed){

            if($friendship->sender_id == $userId){
                return raw(HTML::link(url('friends/confirm/'.$userId.'?redirectTo=friends'), trans('app.confirm')));
            }

            return trans('friends::waiting');
        }

        return trans('app.object_friends');
    }

    /**
     * Check if friendship can be deleted
     *
     * @param  Friendship $friendship The friendshio to check.
     * @param  string $userId   The connected user's id.
     * @return bool
     */
    public function canDelete($friendship, string $userId): bool
    {

        if(is_null($friendship)){
            return false;
        }

        if(!$friendship->confirmed){
            return $friendship->receiver_id == $userId;
        }

        return true;
    }
}
